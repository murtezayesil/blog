+++
title = "Guest Book"
render = true
+++

# Guest Book

{% quote(author="John Doe") %}
Hello
{% end %}

{% quote(author="Vincent", url="https://example.com") %}
A quote
{% end %}