## Shortcodes
### Quotes
```
{% quote( author="John Doe") %}
    Hello, World!
{% end %}
```
> Hello, World!
> -- John Doe

```
{% quote( author="John Doe", url="https://example.com") %}
    Hello, World!
{% end %}
```
> Hello, World!
> -- John Doe [🔗](https://example.com)
