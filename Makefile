BASEDIR=$(CURDIR)
ZOLA?=$(BASEDIR)/bin/zola

help:
	@echo 'Makefile for Zola web site                                                   '
	@echo '                                                                             '
	@echo 'Usage:                                                                       '
	@echo '   make help                                                Print this screen'
	@echo
	@echo '   make build        Builds the website into "public" directory from scratch '
	@echo '          --base-url <base_url>              Overwrite the base_url in config'
	@echo '          --output-dir <output_dir> Put the rendered site into the given path'
	@echo '                                             [default: <project_root>/public]'
	@echo
	@echo '   make check           Check links and rebuild the website without rendering'
	@echo
	@echo '   make init                                        Create a new Zola Project'
	@echo '          <name> Create a project in given name                  [default: .]'
	@echo
	@echo '   make serve      Serve the site. Automatically rebuild and reload on change'
	@echo '          --port <port> to use a different port               [default: 1111]'

build:
	$(ZOLA) build

check:
	$(ZOLA) check

init:
	$(ZOLA) init

serve:
	$(ZOLA) serve
